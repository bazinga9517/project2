import mnist
import os
import numpy as np
import pickle
import cv2
from scipy import ndimage
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.preprocessing import OneHotEncoder, LabelEncoder


def getBestShift(img):
    cy,cx = ndimage.measurements.center_of_mass(img)

    rows,cols = img.shape
    shiftx = np.round(cols/2.0-cx).astype(int)
    shifty = np.round(rows/2.0-cy).astype(int)

    return shiftx,shifty
    
def shift(img,sx,sy):
    rows,cols = img.shape
    M = np.float32([[1,0,sx],[0,1,sy]])
    shifted = cv2.warpAffine(img,M,(cols,rows))
    return shifted  

def preprocessImage(image):
    resizedImg = cv2.resize(image, (20, 20), interpolation=cv2.INTER_NEAREST)
    #mnist.show(resizedImg)
    
    colsPadding = (4,4)
    rowsPadding = (4,4)
    gray = np.lib.pad(resizedImg,(rowsPadding,colsPadding),'constant')
    
    shiftx,shifty = getBestShift(gray)
    shifted = shift(gray,shiftx,shifty)
    
    kernel = np.ones((3,3), dtype=np.float)
    dilatedImg = cv2.dilate(shifted, kernel)
    
    #mnist.show(dilatedImg)
    
    blurredImg = cv2.blur(dilatedImg, (2,2))

    #mnist.show(blurredImg)
        
    return blurredImg

def rotateAndFlip(imgs):
    (n, rows, cols) = imgs.shape
    res = np.zeros((n, rows, cols), dtype = imgs.dtype)
    M = cv2.getRotationMatrix2D((cols/2,rows/2),270,1)
    for i in range(n):
        tmp = cv2.warpAffine(imgs[i,:,:],M,(cols,rows))
        res[i,:,:] = cv2.flip(tmp, 1)
    return res

def mirror(imgs):
    augmentedImages = np.zeros(imgs.shape)
    
    for i in range(imgs.shape[0]):
        augmentedImages[i,:,:] = cv2.flip(imgs[i,:,:], 1)
    
    return np.vstack((imgs, augmentedImages))
        

def augmentSymmetricImages(symbolsImgs, count=7000):
    n = symbolsImgs.shape[0]
    
    print(str(n) + ' to ' + str(count))
    
    if n > count:
        selected = symbolsImgs[np.random.choice(n, count, replace=False),:,:]
    elif 2*n > count: 
        augmentingIndices = np.random.choice(n, count - 2*n, replace=False)
        augmentedImgs = np.array([cv2.flip(img, 1) for img in symbolsImgs[augmentingIndices,:,:]])
        selected = np.vstack((symbolsImgs, augmentedImgs))
    elif 4*n > count:
        augmentedImgs = np.array([cv2.flip(img, 1) for img in symbolsImgs])
        selected = np.vstack((symbolsImgs, augmentedImgs))
        augmentingIndices = np.random.choice(2*n, count - 4*n, replace=False)
        augmentedImgs = np.array([cv2.flip(img, 0) for img in symbolsImgs[augmentingIndices,:,:]])
        selected = np.vstack((symbolsImgs, augmentedImgs))
    else:
        raise ValueError("Not enough data points")
    return selected
    
def getMappingFromFile(fileName):
    d = {}
    with open(fileName, 'r') as f:
        for line in f:
            s = line.split()
            d[int(s[0])] = int(s[1])
    
    return d
        
 # 7000 samples per class
def gatherDataset(symbols =['+', '-', '='], characters = ['a', 'b', 'c', 'd', 'e', 'f']):
    
    if(os.path.isfile(os.path.join(os.path.dirname(__file__), 'data.npz'))):
        print('Importing a saved dataset: data.npz')
        data = np.load(os.path.join(os.path.dirname(__file__), 'data.npz'))
        imgs = data['images']
        labels = data['labels']
        selSymbols = symbols + characters + [str(i) for i in range(10)]
        le = LabelEncoder().fit(selSymbols)
        ohe = OneHotEncoder(sparse=False).fit(np.reshape(le.transform(selSymbols), (-1, 1)))
        selectedIndex = np.array([l in selSymbols for l in labels])
        return (imgs[selectedIndex,:,:], labels[selectedIndex], le, ohe)
    
    print('Importing MNIST...')
    (imgs, labels) = mnist.read(dataset = 'training')
    (imgsTemp, labelsTemp) = mnist.read(dataset = 'testing')
    imgs = np.vstack((imgs, imgsTemp))
    labels = np.concatenate((labels, labelsTemp))
    imgs = imgs/255.0
    #mnist.show(imgs[234])
    
    print('Imported MNIST: ' + str(len(imgs)) + ' images')
    print('Importing mathematical notation...') 
    
    crohmeIdx = []
    crohmeDict = {}
    with open(os.path.join(os.path.dirname(__file__), 'data\\crohme\\classes.txt'), 'r') as f:
        i=0
        for line in f:
            if line.strip() in symbols:
                crohmeIdx.append(i)
                crohmeDict[i] = line.strip()
            i = i+1

    with open(os.path.join(os.path.dirname(__file__), 'data\\crohme\\outputs\\train\\train.pickle'), 'rb') as f:
        crohmeRaw = pickle.load(f)
        
        i = 0
        for trainingSample in crohmeRaw:
            label = np.argmax(trainingSample['label'])
            if(label in crohmeIdx):
                i = i+1
        n = i     
        symbolsImgs = np.zeros((i, 28, 28))
        symbolsLabels = np.zeros((i,), dtype=np.character)
        i = 0
        
        for trainingSample in crohmeRaw:
            label = np.argmax(trainingSample['label'])
            if(label in crohmeIdx):
                image = 1-trainingSample['features']
                
                newImg = preprocessImage(np.reshape(image, (20, 20)))

                symbolsImgs[i,:,:] = newImg

                symbolsLabels[i] = crohmeDict[label]
                
                if(i % 1000 == 0):
                    print(str(i*100.0/n) + '%')
                
                i = i+1
    
    for s in np.unique(symbolsLabels):
        indices = np.where(np.array(symbolsLabels) == s)[0]
        newImages = augmentSymmetricImages(symbolsImgs[indices,:,:])
        imgs = np.vstack((imgs, newImages))
        labels = np.concatenate((labels,[s]*7000))
    
    print('Done importing symbols images.. \nNow importing characters')
    
    charIndices = [ord(c) for c in characters]
    labelToCharMapping = getMappingFromFile(os.path.join(os.path.dirname(__file__), 'data\\emnist\\emnist-bymerge-mapping.txt'))
    
    (charImgs, charLabels) = mnist.read(dataset = 'training', path = "data\\emnist\\", 
            trainingFileNames = ['emnist-bymerge-train-images-idx3-ubyte', 'emnist-bymerge-train-labels-idx1-ubyte'], 
            testingFileNames = ['emnist-bymerge-test-images-idx3-ubyte', 'emnist-bymerge-test-labels-idx1-ubyte'])
    
    charLabels = np.array([labelToCharMapping[l] for l in charLabels])
    validLabels = [charLabels[i] in charIndices for i in range(len(charLabels))]
    
    charImgs = charImgs[validLabels]
    charLabels = charLabels[validLabels]
    
    #for j in range(len(charLabels)):
    #    mnist.show(rotateAndFlip(charImgs[j,:,:].reshape(1, 28, 28))[0], chr(charLabels[j]))
        
    (imgsTemp, labelsTemp) = mnist.read(dataset = 'testing', path = "data\\emnist\\", 
            trainingFileNames = ['emnist-bymerge-train-images-idx3-ubyte', 'emnist-bymerge-train-labels-idx1-ubyte'], 
            testingFileNames = ['emnist-bymerge-test-images-idx3-ubyte', 'emnist-bymerge-test-labels-idx1-ubyte'])

    labelsTemp = np.array([labelToCharMapping[l] for l in labelsTemp])
    validLabels = [labelsTemp[i] in charIndices for i in range(len(labelsTemp))]
    
    charImgs = np.vstack((charImgs, imgsTemp[validLabels]))
    charLabels = np.concatenate((charLabels, labelsTemp[validLabels]))
    
    labelToCharMapping = getMappingFromFile(os.path.join(os.path.dirname(__file__), 'data\\emnist\\emnist-byclass-mapping.txt'))
    
    (imgsTemp, labelsTemp) = mnist.read(dataset = 'training', path = "data\\emnist\\", 
            trainingFileNames = ['emnist-byclass-train-images-idx3-ubyte', 'emnist-byclass-train-labels-idx1-ubyte'], 
            testingFileNames = ['emnist-byclass-test-images-idx3-ubyte', 'emnist-byclass-test-labels-idx1-ubyte'])
            
    labelsTemp = np.array([labelToCharMapping[l] for l in labelsTemp])
    validLabels = [labelsTemp[i] in charIndices for i in range(len(labelsTemp))]

    charImgs = np.vstack((charImgs, imgsTemp[validLabels]))
    charLabels = np.concatenate((charLabels, labelsTemp[validLabels]))
        
    (imgsTemp, labelsTemp) = mnist.read(dataset = 'testing', path = "data\\emnist\\", 
            trainingFileNames = ['emnist-byclass-train-images-idx3-ubyte', 'emnist-byclass-train-labels-idx1-ubyte'], 
            testingFileNames = ['emnist-byclass-test-images-idx3-ubyte', 'emnist-byclass-test-labels-idx1-ubyte'])
              
    labelsTemp = np.array([labelToCharMapping[l] for l in labelsTemp])
    validLabels = [labelsTemp[i] in charIndices for i in range(len(labelsTemp))]
    
    charImgs = np.vstack((charImgs, imgsTemp[validLabels]))
    charLabels = np.concatenate((charLabels, labelsTemp[validLabels]))
    
    labelToCharMapping = getMappingFromFile(os.path.join(os.path.dirname(__file__), 'data\\emnist\\emnist-letters-mapping.txt'))
    
    (imgsTemp, labelsTemp) = mnist.read(dataset = 'training', path = "data\\emnist\\", 
            trainingFileNames = ['emnist-letters-train-images-idx3-ubyte', 'emnist-letters-train-labels-idx1-ubyte'], 
            testingFileNames = ['emnist-letters-test-images-idx3-ubyte', 'emnist-letters-test-labels-idx1-ubyte'])
            
    labelsTemp = np.array([labelToCharMapping[l] for l in labelsTemp])
    validLabels = np.logical_or(labelsTemp == ord('c'), labelsTemp == ord('f'))

    charImgs = np.vstack((charImgs, imgsTemp[validLabels]))
    charLabels = np.concatenate((charLabels, labelsTemp[validLabels]))
        
    (imgsTemp, labelsTemp) = mnist.read(dataset = 'testing', path = "data\\emnist\\", 
            trainingFileNames = ['emnist-letters-train-images-idx3-ubyte', 'emnist-letters-train-labels-idx1-ubyte'], 
            testingFileNames = ['emnist-letters-test-images-idx3-ubyte', 'emnist-letters-test-labels-idx1-ubyte'])
              
    labelsTemp = np.array([labelToCharMapping[l] for l in labelsTemp])
    validLabels = np.logical_or(labelsTemp == ord('c'), labelsTemp == ord('f'))
    
    charImgs = np.vstack((charImgs, imgsTemp[validLabels]))
    charLabels = np.concatenate((charLabels, labelsTemp[validLabels]))
    
    charImgs = charImgs/255.0
    
    for s in np.unique(charLabels):
        print(chr(s))
        indices = np.where(np.array(charLabels) == s)[0]
        charImgs_s = charImgs[indices,:,:]
        #if(charImgs_s.shape[0] < 7000):
        #    print('Augmenting character ' + str(chr(s)))
        #    charImgs_s = mirror(charImgs_s)
        print(charImgs_s.shape)
        newImages = rotateAndFlip(charImgs_s[np.random.choice(len(indices), 7000, replace=False),:,:])
        imgs = np.vstack((imgs, newImages))
        labels = np.concatenate((labels,[chr(s)]*7000))
    
    #print(charImgs.shape)
    #print("N classes: " + str([np.sum(charLabels == c) for c in np.unique(charLabels)]))
    #for i in range(100):
    #    print(chr(charLabels[i]))
    #    mnist.show(charImgs[i])
    
    le = LabelEncoder().fit(l)
    
    ohe = OneHotEncoder(sparse=False).fit(np.reshape(le.transform(l), (-1, 1)))
    
    np.savez_compressed(os.path.join(os.path.dirname(__file__), 'data.npz'), images = imgs, 
        labels = labels)
    
    return (imgs, labels, le, ohe)
    

class Dataset:
    def __init__(self, symbols =['+', '-', '='], characters = ['a', 'b', 'c', 'd', 'e', 'f']):
        (i,l, le, ohe) = gatherDataset(symbols = symbols, characters = characters)
        
        l = le.transform(l).reshape(-1, 1)
        l = ohe.transform(l)
        
        sss = StratifiedShuffleSplit(n_splits=1, test_size = 1.0/7)
        train_index, test_index = list(sss.split(i, np.argmax(l, axis=1)))[0]
        self.imgTrain = i[train_index]
        self.labTrain = l[train_index]
        
        self.imgTest = i[test_index]
        self.labTest = l[test_index]
        self.nClasses = 10+len(symbols)+len(characters)
        self.trIndex = 0
        self.teIndex = 0
        
    def getClasses(self):
        return self.nClasses
        
    def getTrainBatch(self, size):
        if(self.trIndex + size > self.labTrain.shape[0]):
            retImgs = np.vstack((self.imgTrain[self.trIndex:], self.imgTrain[:size - self.labTrain.shape[0] + self.trIndex]))
            retLabs = np.vstack((self.labTrain[self.trIndex:], self.labTrain[:size - self.labTrain.shape[0] + self.trIndex]))
        else:
            retImgs = self.imgTrain[self.trIndex:self.trIndex + size]
            retLabs = self.labTrain[self.trIndex:self.trIndex + size]
        
        self.trIndex = (self.trIndex + size) % self.labTrain.shape[0]
        
        return [retImgs, retLabs]
        
    def getTestBatch(self, size):
        if(self.teIndex + size > self.labTest.shape[0]):
            retImgs = np.vstack((self.imgTest[self.teIndex:], self.imgTest[:size - self.labTest.shape[0] + self.teIndex]))
            retLabs = np.vstack((self.labTest[self.teIndex:], self.labTest[:size - self.labTest.shape[0] + self.teIndex]))
        else:
            retImgs = self.imgTest[self.teIndex:self.teIndex + size]
            retLabs = self.labTest[self.teIndex:self.teIndex + size]
        
        self.teIndex = (self.teIndex + size) % self.labTest.shape[0]
        
        return [retImgs, retLabs]
        
    def getTrainData(self):
        return [self.imgTrain, self.labTrain]
        
    def getTestData(self):
        return [self.imgTest, self.labTest]
        
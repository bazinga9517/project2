import numpy as np
import math

def solve(variables, matrix, constants):
    # get number of functions and the rank of the matrix
    a = np.array(matrix)
    fun, var, rank = len(matrix),len(matrix[0]), np.linalg.matrix_rank(a)

    # if functions are less than variables, return Insufficient functions
    if rank < var:
        return ['There is no unique solution.']
    # if there are more functions that we need, pop the one
    elif fun > var and rank == var:
        for i in range(fun - var):
            matrix.pop(-1)
            constants.pop(-1)

    # get the solution
    m = np.array(matrix)
    y = np.array(constants)
    x = np.linalg.solve(m, y)

    solution = []
    for i in range(len(variables)):
        out = variables[i] + ' = ' + str(x[i])
        solution.append(out)

    return solution

## Abstract ##

This project is to recognize and solve a handwritten linear system from images. We gonna detect what is written, what are the variables and their coresponding scalses using Optimal Bellman Residule and Converlutional Neural Network. Then we gonna use Numpy to solve the matrix and show the result on the screen


## Goal and Tasks ##

https://bitbucket.org/bazinga9517/project2/src/master/COMP9517%20Project%202.pdf?viewer=pdf-viewer


## Final Report

https://bitbucket.org/bazinga9517/project2/src/master/final-report-handwritten.pdf?viewer=pdf-viewer

The report includes designing, algorithms, testing, results, analysis etc.


## How to use it ##

### 1. Clone our repo ###

### 2. CD to the cloned repo ###

### 3-1. run in terminal and take picture with your phone where HOST='' and PORT = 9876. ###

> In this case, you need to turn on your bluetooth or WI-FI to connect your phone with your computer first.

> Then run

>> `python3 python3 main.py`

> and take a photo with your phone. This method require your phone has some specific software to take pictures.

### 3-2. run in terminal and use taken pictures on your local devices. ###

> In this case, you need to run as:

>> `python3 main.py test00.jpg`

> where 'test00.jpg' is the picture on your machine.

### 4. For each step, the result will show one screen. Press 'Enter/Return' to continue till the end. ###


## Contributors ##

Yufei Zhnag

Mattia Gentil

Jieyan Hu

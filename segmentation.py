import cv2
from copy import deepcopy as dc
import imutils
import numpy as np
import mnist


def get_component(graph, k, visited):
    if k in graph:
        if k not in visited:
            visited.append(k)
        for n in graph[k]:
            if n not in visited:
                visited.append(n)
                visited = get_component(graph, n, visited)
    return visited


def inside(t, region):
    tl, br = region[0], region[1]
    if tl[0] <= t[0] <= br[0] and tl[1] <= t[1] <= br[1]:
        return True
    return False


def get_perc(im):
    not_0 = 0
    rows, cols = len(im), len(im[0])
    for r in range(rows):
        for c in range(cols):
            if im[r][c] != 0:
                not_0 += 1
    return not_0 / (rows * cols)

def combine_list(source, chars):
    tl_ws, tl_hs, br_ws, br_hs = [], [], [], []
    for s in source:
        tl_ws.append(chars[s][0][0])
        tl_hs.append(chars[s][0][1])
        br_ws.append(chars[s][1][0])
        br_hs.append(chars[s][1][1])

    tl = (min(tl_ws), min(tl_hs))
    br = (max(br_ws), max(br_hs))
    center = ((tl[0] + br[0]) // 2, (tl[1] + br[1]) // 2)

    return tl, br, center


def segment(image):
    # --------------------------------------------------------------------------
    # read the image and resize it
    img = dc(image)
    if len(img) > 500:
        img = imutils.resize(img, height=500)

    # convert to gray image and find edges
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (3, 3), 0)
    edged = cv2.Canny(blur, 75, 175)
    # cv2.imshow('edged', edged)
    # if cv2.waitKey() & 0xFF == ord('q'):
    #     cv2.destroyAllWindows()

   # --------------------------------------------------------------------------
    # convert the image to binary and get counters
    im2, contours, hierarchy = cv2.findContours(edged, cv2.RETR_LIST,
                                                cv2.CHAIN_APPROX_SIMPLE)
    # --------------------------------------------------------------------------
    # set a dictionary to store all of the characters and get the minimum height
    # of a character
    chars = {}
    for i in range(len(contours)):
        cnt = contours[i]
        x, y, w, h = cv2.boundingRect(cnt)
        chars[((x + x + w) // 2, (y + y + h) // 2)] = [(x, y), (x + w, y + h)]
        # cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 2)

    # --------------------------------------------------------------------------
    # if center is inside a rectangle combine those rectangles
    # 1. find those rectangles that need to be combine
    keys = list(chars.keys())
    need_to_combine = {}
    for k in keys:
        tl, br = chars[k]
        bl, tr = (tl[0], br[1]), (br[0], tl[1])
        for c in chars:
            if k != c and (inside(tl, chars[c]) or inside(br, chars[c]) or
                               inside(bl, chars[c]) or inside(tr, chars[c])):
                # k will combine with c
                if k not in need_to_combine:
                    need_to_combine[k] = [c]
                else:
                    need_to_combine[k].append(c)

    # 2. find all components depends on the need to combine graph
    components = []
    keys = list(need_to_combine.keys())
    for k in keys:
        if k in need_to_combine:
            component = sorted(get_component(need_to_combine, k, []))
            if component is not []:
                components.append(component)
            for n in component:
                if n in need_to_combine:
                    del need_to_combine[n]

    # 3. for all rectangles in components, combine them
    new_center = {}
    for cp in components:
        tl, br, center = combine_list(cp, chars)
        can_add = True
        for key in new_center:
            if abs(center[0] - key[0]) <= 5 and abs(center[1] - key[1]) <= 5:
                can_add = False
                break
        if can_add:
            new_center[center] = [tl, br]

    # 4. for all rectangles in components, remove them form chars
    for cp in components:
        for n in cp:
            if n in chars:
                del chars[n]

    # 5. add new combined rectangles to chars
    for nc in new_center:
        chars[nc] = new_center[nc]
        # cv2.rectangle(img, chars[nc][0], chars[nc][1], (255, 0, 0), 2)

    # cv2.imshow('Combined Connected Counters', img)
    # if cv2.waitKey() & 0xFF == ord('q'):
    #     cv2.destroyAllWindows()

    # --------------------------------------------------------------------------
    # get minimum width of chars and max height of chars
    # 1. find the minimum and max width and height of a character
    keys = list(chars.keys())
    min_w, max_w, min_h, max_h = 10000, 0, 10000, 0
    for c in chars:
        min_w = min(chars[c][1][0] - chars[c][0][0], min_w)
        max_w = max(chars[c][1][0] - chars[c][0][0], max_w)
        min_h = min(chars[c][1][1] - chars[c][0][1], min_h)
        max_h = max(chars[c][1][1] - chars[c][0][1], max_h)

    # 2. find all minus that should combine to equals
    equals = {}
    for k in keys:
        for c in chars:
            delta_w, delta_h = abs(c[0] - k[0]), abs(c[1] - k[1])
            if delta_w < min_w and min_h // 2 < delta_h < max_h // 2:
                equals[k] = [c, k]

    # 3. combine minus to '='
    new_equals = {}
    for e in equals:
        tl, br, center = combine_list(equals[e], chars)
        new_equals[center] = [tl, br]

    # 4. delete
    for e in equals:
        if e in chars:
            del chars[e]

    # 5. add new equals
    for ne in new_equals:
        chars[ne] = new_equals[ne]

    # -------------------------------------------------------------------------
    # decide which characters should be in one line
    lines = []
    for ch in chars:
        if lines is []:
            lines.append([ch])
        else:
            row, new_row = 0, True
            for i in range(len(lines)):
                if abs(ch[1] - lines[i][0][1]) < (max_h + min_h) * 2 // 3:
                    row, new_row = i, False
                    lines[row].append(ch)
                    break
            if new_row:
                lines.append([ch])

    # --------------------------------------------------------------------------
    # for each line, sort it
    fcel = [] # first character each line
    for i in range(len(lines)):
        fcel.append(lines[i][0])

    fcel = sorted(fcel, key=lambda tup: (tup[1]))

    # final_list is an ordered list that we need to show.
    final_list = []
    for point in fcel:
        for i in range(len(lines)):
            if lines[i][0] == point:
                lines[i] = sorted(lines[i])
                final_list.append(lines.pop(i))
                break
    # k = 0
    # for line in final_list:
    #     for c in line:
    #         if k < 10:
    #             cv2.rectangle(img, chars[c][0], chars[c][1], (0, 255, 0), 2)
    #         k += 1
    # cv2.imshow('First 10 Counters Ordered', img)
    # if cv2.waitKey() & 0xFF == ord('q'):
    #     cv2.destroyAllWindows()

    # --------------------------------------------------------------------------
    # We need to resize each character and maybe write them as files
    min_require = 0.1377551020408163  # max percentage of not 0 pixel
    output, boxes = [], []
    k = 0
    row = 1
    for i in range(len(final_list)):
        line, line_b, = [], []
        col = 1
        for c in final_list[i]:
            tl, br = chars[c][0], chars[c][1]
            char = img[tl[1]:br[1], tl[0]:br[0]]
            char = cv2.cvtColor(char, cv2.COLOR_BGR2GRAY)

            # adaptiveThreshold it
            char = cv2.adaptiveThreshold(char, 255,
                                         cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                         cv2.THRESH_BINARY_INV, 77, 15)

            # resize the original image and keep all black pixels
            if len(char) > len(char[0]):
                char = imutils.resize(char, height=20)
            else:
                char = imutils.resize(char, width=20)

            #cv2.imshow('initial ' + str(k), char)
            # bold the image if we need
            while get_perc(char) < min_require:  # min_require:
                char = cv2.dilate(char, (2, 2))
            #cv2.imshow('bold ' + str(k), char)

            char = (char > 0).astype(np.float32)

            # create an empty image as output
            ch = np.zeros((28, 28), dtype=np.float32)
            h = 14 - len(char) // 2
            w = 14 - len(char[0]) // 2
            ch[h:h + len(char), w:w + len(char[0])] = char

            # blur the 28 x 28 image to put to CNN
            ch = cv2.blur(ch, (2, 2))

            # append the image
            line.append(ch)
            line_b.append([tl, br])
            k += 1

            #cv2.imshow('('+str(row) + ', ' + str(col) + ')', ch)
            col += 1
        row += 1

        output.append(np.array(line))
        boxes.append(line_b)

    # --------------------------------------------------------------------------
    # if cv2.waitKey() & 0xFF == ord('q'):
    #     cv2.destroyAllWindows()

    return output, boxes, img

# -*- coding: utf-8 -*-
"""
Created on Mon Oct 16 22:34:26 2017

@author: kelse
"""

import cv2
from PIL import Image,ImageFilter
import numpy as np
from scipy.ndimage.filters import rank_filter
from operator import itemgetter



def props_for_contours(contours, ary):
    """Calculate bounding box & the number of set pixels for each contour."""
    c_info = []
    for c in contours:
        x,y,w,h = cv2.boundingRect(c)
        c_im = np.zeros(ary.shape)
        cv2.drawContours(c_im, [c], 0, 255, -1)
        c_info.append({
            'x1': x,
            'y1': y,
            'x2': x + w - 1,
            'y2': y + h - 1,
            'sum': np.sum(ary * (c_im > 0))/255
        })
    return c_info


def union_crops(crop1, crop2):
    """Union two (x1, y1, x2, y2) rects."""
    x11, y11, x21, y21 = crop1
    x12, y12, x22, y22 = crop2
    return min(x11, x12), min(y11, y12), max(x21, x22), max(y21, y22)


def intersect_crops(crop1, crop2):
    x11, y11, x21, y21 = crop1
    x12, y12, x22, y22 = crop2
    return max(x11, x12), max(y11, y12), min(x21, x22), min(y21, y22)


def crop_area(crop):
    x1, y1, x2, y2 = crop
    return max(0, x2 - x1) * max(0, y2 - y1)


def find_border_components(contours, ary):
    borders = []
    #get the size of image
    area = ary.shape[0] * ary.shape[1]
    for i, c in enumerate(contours):
        x,y,w,h = cv2.boundingRect(c)
        #just select the part whose size is larger than the total size
        if w * h > 0.5 * area:
            borders.append((i,((w-1)*(h-1)), x, y, x + w - 1, y + h - 1))
    return borders


def angle_from_right(deg):
    return min(deg % 90, 90 - (deg % 90))


def remove_border(contour, ary):
    """Remove everything outside a border contour."""
    # Use a rotated rectangle (should be a good approximation of a border).
    # If it's far from a right angle, it's probably two sides of a border and
    # we should use the bounding box instead.
    c_im = np.zeros(ary.shape)
    r = cv2.minAreaRect(contour)
    degs = r[2]
    if angle_from_right(degs) <= 10.0:
        box = cv2.boxPoints(r)
        box = np.int0(box)
        cv2.drawContours(c_im, [box], 0, 255, -1)
        cv2.drawContours(c_im, [box], 0, 0, 4)
    else:
        x1, y1, x2, y2 = cv2.boundingRect(contour)
        cv2.rectangle(c_im, (x1, y1), (x2, y2), 255, -1)
        cv2.rectangle(c_im, (x1, y1), (x2, y2), 0, 4)

    return np.minimum(c_im, ary)


def find_components(edges):
    """Dilate the image until there are just a few connected components.
    Returns contours for these components."""
    # Perform increasingly aggressive dilation until there are just a few
    # connected components.
#    _,contours, hierarchy = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    _,contours, _= cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    
#    Image.fromarray(edges).show()
#    Image.fromarray(255 * dilated_image).show()
    return contours


def find_optimal_components_subset(contours, edges):
    """Find a crop which strikes a good balance of coverage/compactness.
    Returns an (x1, y1, x2, y2) tuple.
    """
    c_info = props_for_contours(contours, edges)
    c_info.sort(key=lambda x: -x['sum'])
    total = np.sum(edges) / 255
    area = edges.shape[0] * edges.shape[1]

    c = c_info[0]
    del c_info[0]
    this_crop = c['x1'], c['y1'], c['x2'], c['y2']
    crop = this_crop
    covered_sum = c['sum']

    while covered_sum < total:
        changed = False
        recall = 1.0 * covered_sum / total
        prec = 1 - 1.0 * crop_area(crop) / area
        f1 = 2 * (prec * recall / (prec + recall))
        #print '----'
        for i, c in enumerate(c_info):
            this_crop = c['x1'], c['y1'], c['x2'], c['y2']
            new_crop = union_crops(crop, this_crop)
            new_sum = covered_sum + c['sum']
            new_recall = 1.0 * new_sum / total
            new_prec = 1 - 1.0 * crop_area(new_crop) / area
            new_f1 = 2 * new_prec * new_recall / (new_prec + new_recall)

            # Add this crop if it improves f1 score,
            # _or_ it adds 25% of the remaining pixels for <15% crop expansion.
            # ^^^ very ad-hoc! make this smoother
            remaining_frac = c['sum'] / (total - covered_sum)
            if crop.__len__()==0:
                break
            new_area_frac = 1.0 * crop_area(new_crop) / crop_area(crop) - 1
            if new_f1 > f1 or (remaining_frac > 0.25 and new_area_frac < 0.15):
                crop = new_crop
                covered_sum = new_sum
                del c_info[i]
                changed = True
                break

        if not changed:
            break

    return crop


def pad_crop(crop, contours, edges, border_contour, pad_px=15):
    """Slightly expand the crop to get full contours.
    This will expand to include any contours it currently intersects, but will
    not expand past a border.
    """
    bx1, by1, bx2, by2 = 0, 0, edges.shape[0], edges.shape[1]
    if border_contour is not None and len(border_contour) > 0:
        c = props_for_contours([border_contour], edges)[0]
        bx1, by1, bx2, by2 = c['x1'] + 5, c['y1'] + 5, c['x2'] - 5, c['y2'] - 5

    def crop_in_border(crop):
        x1, y1, x2, y2 = crop
        x1 = max(x1 - pad_px, bx1)
        y1 = max(y1 - pad_px, by1)
        x2 = min(x2 + pad_px, bx2)
        y2 = min(y2 + pad_px, by2)
        return crop

    crop = crop_in_border(crop)

    c_info = props_for_contours(contours, edges)
    changed = False
    for c in c_info:
        this_crop = c['x1'], c['y1'], c['x2'], c['y2']
        this_area = crop_area(this_crop)
        int_area = crop_area(intersect_crops(crop, this_crop))
        new_crop = crop_in_border(union_crops(crop, this_crop))
        if 0 < int_area < this_area and crop != new_crop:
            changed = True
            crop = new_crop
    if changed:
        return pad_crop(crop, contours, edges, border_contour, pad_px)
    else:
        return crop


#def downscale_image(im, max_dim=1024):
#    """Shrink im until its longest dimension is <= max_dim.
#    Returns new_image, scale (where scale <= 1).
#    """
#    a, b = im.size
#    if max(a, b) <= max_dim:
#        return 1.0, im
#
#    scale = 1.0 * max_dim / max(a, b)
#    return scale, im


def process_image(path):
<<<<<<< HEAD
    orig_im = Image.fromarray(path)
    im=orig_im
    # im =orig_im.filter(ImageFilter.DETAIL)
    orig_im=im.copy()
=======
    orig_im =Image.fromarray(path)
#    im =orig_im.filter(ImageFilter.DETAIL)
    im=orig_im.copy()
    a,b=orig_im.size
>>>>>>> 1aeb9c85d7d867279ca73650228d8a831f219028
    edges = cv2.Canny(np.asarray(im), 100, 200)
    # TODO: dilate image _before_ finding a border. This is crazy sensitive!
    _,contours, _ = cv2.findContours(edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    borders = find_border_components(contours, edges)
    borders=sorted(borders,key=itemgetter(1))
    borders=sorted(borders,key=itemgetter(0))

    border_contour = None
    if len(borders):
        border_contour = contours[borders[0][0]]
        edges = remove_border(border_contour, edges)
#    cv2.imwrite('before.jpg',edges)
    edges = 255 * (edges > 0).astype(np.uint8)
    # Remove ~1px borders using a rank filter.
    maxed_rows = rank_filter(edges, -4, size=(50,1))
    maxed_cols = rank_filter(edges, -4, size=(1, 50))
    edges= np.minimum(np.minimum(edges, maxed_rows), maxed_cols)
    contours = find_components(edges)
    if len(contours) == 0:
        return

    crop = find_optimal_components_subset(contours, edges)
    crop = pad_crop(crop, contours, edges, border_contour)

    crop = [int(x) for x in crop] # upscale to the original image size.
    crop[0]-=a//32
    crop[1]-=b//32
    crop[2]+=a//32
    crop[3]+=b//32
    if crop[0]<0:
        crop[0]=0
    if crop[1]<0:
        crop[1]=0
    if crop[2]>a:
        crop[2]=a
    if crop[3]>b:
        crop[3]=b
    text_im = orig_im.crop(crop)
    # lighten
    return np.array(text_im,dtype=np.uint8) 


if __name__ == '__main__':
    path=cv2.imread('succeed04.jpg')
#    out_path = path.replace('.jpg', '.crop.jpg')
    img=process_image(path)
    cv2.imwrite('12.jpg',img)
       
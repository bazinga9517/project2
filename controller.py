import segmentation as sgm
import recognization as recg
import build_system as bs
import solve_matrix as sm
import display as dis
#import video_photo as vp
import trr 

def grab(camera):
    return trr.process_image(camera)

def segment(image):
    return sgm.segment(image)


def recognize(image):
    return recg.recognize(image)


def build_system(system):
    return bs.build(system)


def solve(variables, matrix, constants):
    return sm.solve(variables, matrix, constants)


def display(out_sys, system, out_sol, solution):
    dis.show(out_sys, system, 'system')
    dis.show(out_sol, solution, 'solution')

import numpy as np


def update(record, cur, scale, rows, r, simble):
    if cur not in record:
        record[cur] = np.zeros(rows)
    record[cur][r] = scale
    return record, True, simble, 0


def read_line(line, record, vec, rows, r):
    chars = line.split()
    is_num, cur, scale = True, '', 0

    for ch in chars:
        if ch == '-':
            if is_num:
                cur += ch
            else:
                record, is_num, cur, scale = update(record, cur, scale, rows, r,
                                                    '-')
        elif ch == '+':
            record, is_num, cur, scale = update(record, cur, scale, rows, r, '')
        elif ch.isdigit():
            cur += ch
        elif ch.isalpha():
            if cur == '-':
                cur = '-1'
            if cur == '':
                cur = '1'
            if is_num:
                scale = float(cur)
            cur, is_num = ch, False

        elif ch == '=':
            record, is_num, cur, scale = update(record, cur, scale, rows, r, '')

    try:
        cur = float(cur)
    except ValueError:
        print("Cannot convert string to fload " + cur)
        return {}, []

    vec.append(cur)

    return record, vec


def build(system):
    variables, matrix, constants = [], [], []
    record, rows = {}, len(system)

    for r in range(rows):
        record, constants = read_line(system[r], record, constants, rows, r)

    variables = list(record.keys())

    for r in range(rows):
        cur = []
        for v in variables:
            cur.append(record[v][r])
        matrix.append(cur)

    return variables, matrix, constants

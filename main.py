import cv2
import numpy as np
import controller as ctl
import train
import serverSocket
import mnist
import argparse
import trr
import sys


if __name__ == '__main__':
    # --------------------- input using phone camera ------------------------
    importFromAndroid = True if len(sys.argv) == 1 else False
    
    image = serverSocket.getImage() if importFromAndroid else cv2.imread(sys.argv[1])

    # --------------------- segmentation -----------------------
    output, boxes, img = ctl.segment(image)

    k = 0
    for line in boxes:
        for b in line:
            cv2.rectangle(img, b[0], b[1], (0, 255, 0), 2)
            k += 1
    cv2.imshow('segmentation ' + str(k), img)
    if cv2.waitKey() & 0xFF == ord('q'):
        cv2.destroyAllWindows()

    newImg = np.ones((img.shape[0]+100, img.shape[1], img.shape[2]), dtype=img.dtype)
    newImg[:img.shape[0], :, :] = img
    img = newImg

    # --------------------- recog nition ------------------------
    cnn = train.CNN(m='./cnn1')
    system = []
    for i in range(len(output)):
        line = ''
        for j in range(len(output[i])):
            tt = output[i][j]

            result = cnn.eval(tt)
            print(i, j, result[1], result[0]*100)

            if j%2 == 0:
                offset = 0
            else:
                offset = 13

            img = cv2.putText(img, str(result[1][0]), (boxes[i][j][0][0], boxes[i][j][1][1]+25+offset),
            cv2.FONT_HERSHEY_SIMPLEX, fontScale=0.6, color=(150, 0,0), thickness=1)

            img = cv2.putText(img,format(result[0][0]*100,".1f")+"%",(boxes[i][j][0][0]+15,boxes[i][j][1][1]+25+offset),
            cv2.FONT_HERSHEY_SIMPLEX,fontScale=0.3,color=(0,0, 150),thickness=1)


            #mnist.show(tt)
            line += result[1][0] + ' '
        system.append(line)

    cv2.imshow('Predictions', cv2.resize(img, (1366, 768)))
    cv2.waitKey()

    # --------------------- build matrix ------------------------
    variables, matrix, constants = ctl.build_system(system)

    # --------------------- solve matrix ------------------------
    solution = ctl.solve(variables, matrix, constants)

    # --------------------- display outputs ------------------------
    out_sys = np.zeros((720, 1080, 3), np.uint8)
    out_sol = np.zeros((500 , 900, 3), np.uint8)
    ctl.display(out_sys, system, out_sol, solution)

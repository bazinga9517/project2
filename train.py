import tensorflow as tf
import tfGraph
import dataset
from sklearn.model_selection import KFold
import os
import numpy as np
import time

#export_dir = ".\\cnn\\model" # for windows
model = "./cnn1"
export_dir = model + "/model"

class CNN:
    def __init__(self, m="./cnn1"):
        self.sess = tf.Session()
        new_saver = tf.train.import_meta_graph(m + "/model.meta")
        # new_saver.restore(self.sess, tf.train.latest_checkpoint('.\\cnn\\')) # for windows
        new_saver.restore(self.sess, tf.train.latest_checkpoint(m)) # for mac
        graph = tf.get_default_graph()
        
        self.x = graph.get_tensor_by_name("image_input:0")
        y_pred = graph.get_tensor_by_name("prediction:0")
        self.keep_prob = graph.get_tensor_by_name("dropout:0")
        
        self.y_classes = tf.nn.softmax(y_pred)
        (_,_,self.le, self.ohe) = dataset.gatherDataset()
        
    def eval(self, image):
        preds = self.y_classes.eval(feed_dict={self.x: np.reshape(image, (-1, 28, 28)), self.keep_prob: 1.0}, session=self.sess)
        
        confidence = np.max(preds, axis=1)
        chars = self.le.inverse_transform(np.argmax(preds, axis=1))
        
        return (confidence, chars)
        
if __name__ =='__main__':
    data = dataset.Dataset()
    nClasses = data.getClasses()
    
    # Create the model
    x = tfGraph.input_placeholder()
    
    # Define loss and optimizer
    y_ = tfGraph.target_placeholder(nClasses)
    
    # Build the graph for the deep net
    y_conv, keep_prob = tfGraph.lenet5(x, nClasses)
    
    with tf.name_scope('loss'):
        cross_entropy = tf.nn.softmax_cross_entropy_with_logits(labels=y_,
                                                                logits=y_conv)
        cross_entropy = tf.reduce_mean(cross_entropy)
        
    with tf.name_scope('adam_optimizer'):
        train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
    
    with tf.name_scope('accuracy'):
        correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
        correct_prediction = tf.cast(correct_prediction, tf.float32)
        accuracy = tf.reduce_mean(correct_prediction)
    
    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        
        startTime = time.time()
        
        for i in range(20000):
            batch = data.getTrainBatch(50)
            if i % 100 == 0:
                train_accuracy = accuracy.eval(feed_dict={
                    x: batch[0], y_: batch[1], keep_prob: 1.0})
                print('step %d, training accuracy %g' % (i, train_accuracy))
                
            if i % 300 == 0:
                teBatch = data.getTestBatch(100)
                test_accuracy = accuracy.eval(feed_dict={x: teBatch[0], y_: teBatch[1], keep_prob: 1.0})
                print('test accuracy %g' % test_accuracy)
                
                with open(model + '\\log.csv', 'a+') as f:
                    f.write(str(i) + ',' + str(train_accuracy) + ',' + str(test_accuracy) + '\n')
            train_step.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})
    
        
        
        [testImgs, testLabs] = data.getTestData()
        
        testAccuracy = 0.0
        kf = KFold(n_splits = 1330) # Allows batches of max 100 images to avoid memory faults
        for trainIndex, testIndex in kf.split(testImgs):
            
            testAccuracy += len(testIndex) * accuracy.eval(feed_dict={
            x: testImgs[testIndex], y_: testLabs[testIndex], keep_prob: 1.0})
            
        y_conv = tf.identity(y_conv, name='prediction')
        
        saver = tf.train.Saver()
        saver.save(sess, export_dir)
        print('Final test accuracy %g' % (testAccuracy / len(testLabs)))
        with open(model + '\\finalTestAccuracy.txt', 'w+') as f:
            f.write(str(time.time() - startTime) + ',' + str(testAccuracy / len(testLabs)))
            
    tf.reset_default_graph()
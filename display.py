import cv2
import numpy as np


def show(output, system, name):
    # check how many lines are there in a system
    row = len(system)

    # check the maximum length of a line
    max_len = 0
    for line in system:
        max_len = max(max_len, len(line))

    # set spaces
    w, h = 10, 60

    # set font
    font = cv2.FONT_HERSHEY_SIMPLEX

    # show the system
    for i in range(row):
        cv2.putText(output, system[i], (w, h + 2 * h * i), font, 2,
                    (255, 255, 255), 2, cv2.LINE_AA)

    cv2.imshow(name, output)

    if cv2.waitKey() & 0xFF == ord('q'):
        cv2.destroyAllWindows()

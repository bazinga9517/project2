import socket
import sys
import cv2

def getImage():
    HOST = ''   # Symbolic name, meaning all available interfaces
    PORT = 9876 # Arbitrary non-privileged port
    
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print('Socket created')
    
    #Bind socket to local host and port
    try:
        s.bind((HOST, PORT))
    except socket.error as msg:
        print('Bind failed. Error Code : ' + str(msg[0]) + ' Message ' + msg[1])
        sys.exit()
        
    print('Socket bind complete')
    
    #Start listening on socket
    s.listen(10)
    print('Socket now listening')
    
    #now keep talking with the client
    #wait to accept a connection - blocking call
    conn, addr = s.accept()
    print('Connected with ' + addr[0] + ':' + str(addr[1]))
    
    byteStream = b''
    
    while True: 
        data = conn.recv(1024)
        if not data:
            break
            
        byteStream += data
        
    with open('image.jpg', 'wb') as f:
        f.write(byteStream)
        
    print('Read ' + str(len(byteStream)) + ' bytes')
        
    print('Exiting')  
    s.close()
    
    return cv2.imread('image.jpg')
    
if(__name__ == '__main__'):
    img = getImage()
    
    cv2.imshow(None, img)
    cv2.waitKey()
    